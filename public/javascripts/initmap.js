var map;
  var cadastralLayer;
        
        $(document).ready(function () {
          map = L.map('map');

            var tiles = L.tileLayer('http://{s}.tiles.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZWNvdHJ1c3QiLCJhIjoibGo4TG5nOCJ9.QJnT2dgjL4_4EA7WlK8Zkw', {
                maxZoom: 18,
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
                    '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                    'Imagery © <a href="http://mapbox.com">Mapbox</a>',
                id: 'examples.map-i875mjb7'
            });



              map.on('load', function() {
                requestUpdatedCadastre();
              });
              /*map.on('moveend', function() {
                requestUpdatedCadastre();
              });*/
              map.setView([51.505, -0.09], 13).addLayer(tiles);


            });
            
            function requestUpdatedCadastre() {
              $.ajax(
                {
                  type: 'GET',
                  url: '/provinces',
                  dataType: 'json',
                  success: function (result) {
                    parseResponseCadastre(result);
                  },
                  error: function (req, status, error) {
                    alert('Unable to get cadastral data');
                  }
                });
            }
            
            function parseResponseCadastre(data) {
              if (cadastralLayer !== undefined)
              {
                map.removeLayer(cadastralLayer);
              }
              cadastralLayer = new L.GeoJSON(data);
              cadastralLayer.on('featureparse', function(e) {
                e.layer.setStyle({ color:  '#003300', weight: 2, fill: true, fillColor: '#009933' });
              });



              //cadastralLayer.addGeoJSON(data);
              map.addLayer(cadastralLayer);
            }

