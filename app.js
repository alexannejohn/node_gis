
/**
 * Module dependencies.
 */

var express = require('express');
var pg = require('pg');
//var routes = require('./routes');

var app = module.exports = express.createServer();

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});


app.set('view options', {
  layout: false
});


// Routes

//app.get('/', routes.index);

// Routes
app.get('/', function(req, res){
  res.render('index', {
  });
});

app.get('/provinces', function(req, res){
  //console.log("gets to here");
  RetrieveProvinces(req, res);
});

// RetrieveCadastre
function RetrieveProvinces(req, res){

    var connString = "postgres://alex:Challenger1@localhost/alex";
    pg.connect(connString, function(err, client) {

        console.log("gets to here");


        var sql = 'select ST_AsGeoJSON(geom) as shape ';
        sql = sql + 'from forestdata.provinces ';
        
        
        client.query(sql, function(err, result) {
            var featureCollection = new FeatureCollection();

            for (i = 0; i < result.rows.length; i++)
            {
                featureCollection.features[i] = JSON.parse(result.rows[i].shape);
            }

            res.send(featureCollection);
            //console.log(result);
        });
    });
}

// GeoJSON Feature Collection
function FeatureCollection(){
    this.type = 'FeatureCollection';
    this.features = [];
}



app.listen(3000, function(){
  console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});
